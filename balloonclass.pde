import java.lang.Math;

class Balloon {
  PVector pos;
  PVector vel;
  PVector acc;
  double size = 0.8;
  final double GROUND_VOL = 4f/3f*PI*Math.pow(size, 3);
  final double C = 0.00011860103;
  final float G = 9.81;

  double volume = GROUND_VOL;

  float temp = 283;
  float rho0 = 1.225;

  float mass = 0.5;

  float windAMT = 0.1;

  Balloon() {
    pos = new PVector(width/2/scale, height*0.15/scale);
    acc = new PVector();
    vel = new PVector();
  }

  void calc() { //Time in milliseconds
    volume = GROUND_VOL*1*Math.pow(Math.E, C*(-pos.y));
    size = Math.pow(volume/4.0/PI*3.0, 1f/3);
    acc.y = (rho0-(mass/(float)volume)*G*(float)volume-mass*G)/mass;
    wind();
    acc.mult(1f/60);
    vel.add(acc);
    //vel.y = -10;

    PVector v = vel.copy();
    v.mult(1f/60);
    pos.add(v);
    offset = (int)(height*0.5-(pos.y*scale+height*0.7));
  }

  void wind() {
    acc.x = windAMT*noise(500+frameCount*0.1)/mass;
  }

  void display() {
    fill(255, 0, 0);
    displayData();
    //camera(width/2.0, height/2.0, (height/2.0) / tan(PI*30.0 / 180.0), width/2.0, height/2.0, 0, 0, 1, 0)
    //camera(eyeX, eyeY, eyeZ, centerX, centerY, centerZ, upX, upY, upZ)
    camera(pos.x*scale, height/2.0-offset, (height/2.0) / tan(PI*30.0 / 180.0), pos.x*scale, height/2.0-offset, 0, 0, 1, 0);
    ellipse(pos.x*scale, pos.y*scale+height*0.7, (float)size*2*scale, (float)size*2*scale);
    stroke(0);
    line(pos.x*scale, pos.y*scale+height*0.7, pos.x*scale-5, pos.y*scale+height*0.7+100);
    line(pos.x*scale, pos.y*scale+height*0.7, pos.x*scale+5, pos.y*scale+height*0.7+100);
    rectMode(CENTER);
    fill(100);
    rect(pos.x*scale, pos.y*scale+height*0.7+80, 20, 20);
    noStroke();
  }

  void displayData() {
    camera();
    textSize(18);
    textAlign(LEFT, TOP);
    text("Acceleration: "+str(acc.y), 10, 10);
    text("Velocity: "+str(vel.y), 10, 26);
    text("Radius: "+String.valueOf(size), 10, 40);
    text("Altitude: "+str(pos.y), 10, 56);
    text("Wind drift: "+str(width/2.0-pos.x*scale), 10, 72);
  }
}
