
PShape backdrop;
int backdrop_detail = 40;
ArrayList<Cloud> clouds = new 
ArrayList<Cloud>();

Balloon balloon;

float scale = 10;

int offset;

void settings(){
  size(1280, 800, P3D);
  
}

void setup(){
  surface.setTitle("FL04T-Balloon!");
  //surface.setResizable(false);
  delay(50);
  width = 1280;
  height = 720;
  backdrop = createShape();
  backdrop.beginShape();
  backdrop.fill(200, 255, 200);
  backdrop.noStroke();
  backdrop.vertex(-50, height);
  for(int i = 0; i < backdrop_detail; i++){
    backdrop.vertex(width/backdrop_detail*(i+0.5), height-(0.5+noise(i*0.2))*height*0.4);
    //backdrop.vertex(width/backdrop_detail*(i+0.5), height);
  }
  backdrop.vertex(width+50, height);
  backdrop.endShape(OPEN);
  
  balloon = new Balloon();
}

void draw(){
  drawBackground();
  balloon.calc();
  balloon.display();
  if(random(1) < 0.08 && clouds.size() < 30){
    clouds.add(new Cloud(round(random(-height*0.3, -height*0.1))));
    clouds.get(clouds.size()-1).generate();
  }
  pushMatrix();
  translate(0, -offset);
  for(int i = clouds.size()-1; i >= 0; i--){
    
    clouds.get(i).display();
    if(clouds.get(i).life < 1){
      clouds.remove(i);
    }
  }
  popMatrix();
}

void drawBackground(){
  background(150, 150, 255);
  pushMatrix();
  translate(0, 0, -10);
  shape(backdrop, 0, 0);
  popMatrix();
  fill(150);
  noStroke();
}
