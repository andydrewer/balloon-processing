class Cloud{
  PVector pos;
  float w, h;
  final int NUMPARTS = 100;
  CloudPart[] parts = new CloudPart[NUMPARTS];
  
  final int LIFE_INIT = 300;
  int life = LIFE_INIT;
  
  float noiseScale = 0.002;
  float noiseSeed;
  
  Cloud(int y){
    pos = new PVector(random(-width/2.0, width/2.0)+balloon.pos.x*scale, y);
    w = random(50, 120);
    h = random(20, 60);
  }
  
  void generate(){
    int fc = frameCount;
    for(int i = 0; i < NUMPARTS; i++){
      parts[i] = new CloudPart(new PVector(pos.x+abs(random(-1, 1))*w, pos.y+abs(random(-1, 1))*h), random(5, 20));
    }
    
  }
  
  void display(){
    pushMatrix();
    translate(0, 0, -100);
    for(int i = 0; i < NUMPARTS; i++){
      parts[i].display(color(105+(1+noise(100+frameCount*0.01+i*0.04))*(150/2),(life*1f/LIFE_INIT*255)));
      parts[i].pos.y -= balloon.vel.y*0.2;
    }
    popMatrix();
    //println((life/LIFE_INIT*255));
    life--;
  }
}

class CloudPart{
  PVector pos;
  float size;
  
  CloudPart(PVector _p, float _s){
    pos = _p;
    size = _s;
  }
  
  void display(color c){
    fill(c);
    ellipse(pos.x, pos.y, size, size);
  }
}
